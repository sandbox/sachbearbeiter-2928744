BACKGROUND
----------

Altmetrics

In scholarly and scientific publishing, altmetrics are non-traditional metrics proposed as an alternative to more traditional citation impact metrics, such as impact factor and h-index. The term altmetrics was proposed in 2010, as a generalization of article level metrics, and has its roots in the #altmetrics hashtag. Although altmetrics are often thought of as metrics about articles, they can be applied to people, journals, books, data sets, presentations, videos, source code repositories, web pages, etc. They are related to Webometrics, which had similar goals but evolved before the social web. Altmetrics did not originally cover citation counts. It also covers other aspects of the impact of a work, such as how many data and knowledge bases refer to it, article views, downloads, or mentions in social media and news media.

Altmetrics are a very broad group of metrics, capturing various parts of impact a paper or work can have. A classification of altmetrics was proposed by ImpactStory in September 2012, and a very similar classification is used by the Public Library of Science:

 * Viewed - HTML views and PDF downloads
 * Discussed - journal comments, science blogs, Wikipedia, Twitter, Facebook and other social media
 * Saved - Mendeley, CiteULike and other social bookmarks
 * Cited - citations in the scholarly literature, tracked by Web of Science, Scopus, CrossRef and others
 * Recommended - for example used by F1000Prime

(Source: Wikipedia, https://en.wikipedia.org/wiki/Altmetrics)

Lagotto

Traditionally, the impact of research articles has been measured by the publication journal. But a more informative view is one that examines the overall performance and reach of the articles themselves. Article-Level Metrics capture the manifold ways in which research is disseminated and used, including:

 * viewed
 * shared
 * discussed
 * cited
 * recommended.

Each of the metrics fall under one of these activities. But as an entire set, they paint a comprehensive portrait of the overall footprint of how the scholarly and public are engaging with the research.

ALM offer a dynamic and real-time view of the latest activity. Researchers can stay up-to-date with their published work and share information about the impact of their publications with collaborators, funders, institutions, and the research community at large. These metrics are also a powerful way to navigate and discover others’ work. They can be customized based on the unique needs of researchers, publishers, institutional decision-makers, and funders.
Lagotto Application

The Lagotto application collects and aggregates the data from external sources where article activity is occurring. It was developed in 2009 as an open source application by PLOS. To date, the community of contributors to the application has rapidly grown as more publishers and other providers are implementing the system.

(Source: Lagotto, http://www.lagotto.io/)

Please consider, that you need some technical knowledge of Drupal to configure this module. Not all settings are done by the administrative UI. Additionally you have to create and edit an include file in the module directory.

You need this module if you want to submit article metadata from your Drupal installation to DOAJ via JSON upload.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/dara_connector

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/dara_connector


INTRODUCTION
------------

You can use this module to visualize Lagotto JSON files (Article-Level Metrics (ALM)) for scholarly articles in Drupal.
This module uses the d3.js library. An example can be found here: http://jalperin.github.io/almviz/
Be aware: the functionality of this module depends hardly on the right combination of the almwiz scripts and the Lagotto
API version. Feel free to open an issue, if you have questions about the usage of this module.


REQUIREMENTS
------------

This module requires no other modules beside the Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------

 * Navigate to the Settings Page (/admin/config/content/lagotto_metrics)

    - Below "Site-specific Settings" you can link to the PHP include file that defines customized settings for your site.
    - Below "Content Settings" you can choose the content types on which you would like to enable the da|ra Connector functionality.

This module creates a block, that you can fill by creating a site specific include file: example.sitespecific.inc is the base for this.
Please attach the block to the region of your choice below /admin/structure/block .


UNINSTALL
---------

Disable and uninstall the module (all variables in the database are going to be completely removed) and delete the module folder from the modules directory.


MAINTAINERS
-----------

Current maintainers:
 * sachbearbeiter - https://www.drupal.org/u/sachbearbeiter
 * gabor_h - https://www.drupal.org/u/gabor_h

This project has been sponsored by:
 * Alexander von Humboldt Institut für Internet und Gesellschaft (HIIG) 
   The Alexander von Humboldt Institute for Internet and Society (HIIG) 
   explores the dynamic relationship between the Internet and society, 
   including the increasing interpenetration of digital infrastructures 
   and various domains of everyday life. Its goal is to understand the 
   interplay of social-cultural, legal, economic and technical norms in 
   the process of digitalisation.
   Visit https://www.hiig.de/en/ for more information.
   This module is used for the Internet Policy Review project.
   It's an open access, fast track and peer-reviewed journal on internet regulation.
   Visit https://policyreview.info/ for more information.