<?php
/**
 * @file
 *  Site-specific include file for lagotto_metrics.module
 */

/**
 * Get lagotto block content
 * Return HTML of the block or '' if block should not be displayed
 */
function _lagotto_metrics_get_block_contents_sitespecific($node) {
  $output = '';

  // get the DOI name of this node
  $doi = render(field_view_field('node', $node, 'field_comp_doi'));
  // Normally you can make a call to the Lagotto API to get the JSON data. 
  // But in our scenario the Lagotto Server is running in the intranet of the institution 
  // and we get files with the doi as name and acting as an identifier  
  $json_filename = str_replace('/', '_',  $doi);
  $json_filepath = file_create_url('public://') . 'lagotto_json/' . $json_filename . '.json';
  $piwik_filepath = file_create_url('public://') . 'lagotto_json/img/' . $json_filename . '.png';
  
  // Check whether the JSON file will be available to JS code
  // This is not ideal, but it works.
  $file_headers = @get_headers($json_filepath);
  if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
    $output .= '<h2>' . t('Metrics') . '</h2>';
    $output .= '<p>' . t('Metrics are created.') . '</p>';
    return $output;
  }
  
  $output .= '
  <h2 class="js-expandable pointer">
<span class="is-visible"><i class="material-icons md-18 md-center">expand_more</i> ' . t('Metrics') . '</span>
<span class="is-hidden"><i class="material-icons md-18 md-center">expand_less</i> ' . t('Metrics') . '</span>
</h2>
<div class="is-hidden">
<div id="alm"><div id="loading">' . t('Metrics loading...') . '</div></div>
<script type="text/javascript">
    window.theLagottoDataFile = "' . $json_filepath . '";
 </script>
    <div class="alm-group">
        <div class="alm-source with-chart">
            <div class="alm-label discussed">
                <p class="alm-count" id="alm-count-twitter_search-discussed">' . t('Views') . '</p>
            </div>
            <div class="alm-chart"><img src="' . $piwik_filepath . '" alt="' . t('Piwik data for the article with the DOI @doi', array('@doi' => $doi)) . '" /></div>
        </div>
    </div>
</div>
';
  // Add JS and CSS files stored in the "almviz-ipr" subdirectory
  $almviz_path=drupal_get_path('module', 'lagotto_metrics') . '/almviz-ipr';
  
  drupal_add_js($almviz_path . '/js/bootstrap.tooltip.min.js');
  drupal_add_js($almviz_path . '/js/d3.v3.min.js');
  drupal_add_css($almviz_path . '/css/almviz.css'); 
  drupal_add_css($almviz_path . '/css/bootstrap.tooltip.css'); 
  drupal_add_js($almviz_path . '/js/lagotto_alm.js', array(
    'type' => 'file',
    'scope' => 'footer',
    'group' => JS_THEME,
    'weight' => 10,
  ));
  return $output;
}