<?php
/**
 * @file
 *  Lagotto metrics
 */


/**
 * lagotto_metrics settings
 */
function lagotto_metrics_admin_settings($form, &$form_state) {
  $form['sitespecific_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Site-specific Settings'),
    '#collapsible' => TRUE,
  );

  $form['sitespecific_settings']['lagotto_metrics_sitespecific_include_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name of your site-specific include file'),
    '#field_suffix' => ' .sitespecific.inc',
    '#description' => t('This module requires a PHP include file that has been customized for your site. Please see example.sitespecific.inc file in the module directory as an example and follow the directions given in that file. Allowed characters are 0-9, a-z, underscore and dash.'),
    '#default_value' => variable_get('lagotto_metrics_sitespecific_include_name', ''),
  );
  $form['content_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Settings'),
    '#collapsible' => TRUE,
  );
  // The content type machine name is used for the block identifier (called delta).
  // Maximum length of this delta is 32 characters, so do not allow to enable content types 
  // that have machine names longer than 32 chars.
  $node_types = node_type_get_names();
  $supported_node_types=array();
  $unsupported_node_types=array();
  foreach ($node_types as $key => $value) {
    if (strlen($key)<32) {
      $supported_node_types[$key] = $value;
    }
    else {
      $unsupported_node_types[$key]=$value;
    }
  }
  $form['content_settings']['lagotto_metrics_content_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Enabled content types'),
    '#description' => t('Choose the types on which you would like to enable the lagotto_metrics functionality. Choose none to enable it on all content types.'),
    '#options' => $supported_node_types,
    '#default_value' => variable_get('lagotto_metrics_content_types', array()),
  );
  if ($unsupported_node_types) {
    $form['content_settings']['lagotto_metrics_content_types']['#description'] .= '<br />' . t('Unsupported content types (too long names): @list', array('@list' => implode(', ', array_keys($unsupported_node_types))));
  }
  return system_settings_form($form);
}